import unittest
from alertmanager_downtimer import alertmanager_downtimer


class MatcherTestCase(unittest.TestCase):

    def test_set_name_default(self):
        matcher = alertmanager_downtimer.Matcher()
        self.assertEqual('alertname', matcher.get_name())

    def test_set_value_default(self):
        matcher = alertmanager_downtimer.Matcher()
        self.assertEqual('.*', matcher.get_value())

    def test_set_is_regex_default(self):
        matcher = alertmanager_downtimer.Matcher()
        self.assertEqual(True, matcher.get_is_regex())

    def test_set_is_regex_none(self):
        matcher = alertmanager_downtimer.Matcher(is_regex=None)
        self.assertEqual(True, matcher.get_is_regex())

    def test_set_is_regex_false(self):
        matcher = alertmanager_downtimer.Matcher(is_regex=False)
        self.assertEqual(False, matcher.get_is_regex())

    def test_set_is_regex_empty(self):
        matcher = alertmanager_downtimer.Matcher(is_regex='')
        self.assertEqual(False, matcher.get_is_regex())


class AlertmanagerDowntimerTestCase(unittest.TestCase):

    def test_set_url_invalid(self):
        url = 'toto'
        with self.assertRaises(ValueError):
            am_downtimer = alertmanager_downtimer.AlertmanagerDowntimer(
                url=url
            )

    def test_set_url(self):
        url = 'https://example.com'
        am_downtimer = alertmanager_downtimer.AlertmanagerDowntimer(
            url=url
        )
        self.assertEqual(url, am_downtimer.get_url())

    def test_start_hour_default(self):
        url = 'https://example.com'
        am_downtimer = alertmanager_downtimer.AlertmanagerDowntimer(
            url=url
        )
        self.assertEqual(17, am_downtimer.get_start_hour())

    def test_end_hour_default(self):
        url = 'https://example.com'
        am_downtimer = alertmanager_downtimer.AlertmanagerDowntimer(
            url=url
        )
        self.assertEqual(9, am_downtimer.get_end_hour())

    def test_get_auth_empty(self):
        url = 'https://example.com'
        am_downtimer = alertmanager_downtimer.AlertmanagerDowntimer(
            url=url
        )
        self.assertEqual((), am_downtimer.get_auth())

    def test_set_api_version_default(self):
        url = 'https://example.com'
        am_downtimer = alertmanager_downtimer.AlertmanagerDowntimer(
            url=url
        )
        self.assertEqual('v2', am_downtimer.get_api_version())

    def test_set_api_version(self):
        url = 'https://example.com'
        am_downtimer = alertmanager_downtimer.AlertmanagerDowntimer(
            url=url,
            api_version='v1'
        )
        self.assertEqual('v1', am_downtimer.get_api_version())
