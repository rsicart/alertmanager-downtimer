import os
from alertmanager_downtimer import alertmanager_downtimer
import logging

if __name__ == '__main__':

    # enable debug
    if os.environ.get('DEBUG', False):
        logging.basicConfig(level=logging.DEBUG)

    url = os.environ.get('ALERTMANAGER_URL')

    if url is None:
        raise ValueError('ALERTMANAGER_URL env var is empty')

    # not mandatory
    username = os.environ.get('ALERTMANAGER_USERNAME')
    password = os.environ.get('ALERTMANAGER_PASSWORD')
    start_hour = os.environ.get('ALERTMANAGER_START_HOUR')
    end_hour = os.environ.get('ALERTMANAGER_END_HOUR')
    api_version = os.environ.get('ALERTMANAGER_API_VERSION')

    matcher_name = os.environ.get('MATCHER_NAME')
    matcher_value = os.environ.get('MATCHER_VALUE')
    matcher_is_regex = os.environ.get('MATCHER_IS_REGEX')

    matcher = alertmanager_downtimer.Matcher(
        name=matcher_name,
        value=matcher_value,
        is_regex=matcher_is_regex
    )

    am_downtimer = alertmanager_downtimer.AlertmanagerDowntimer(
        url=url,
        username=username,
        password=password,
        start_hour=start_hour,
        end_hour=end_hour,
        matcher=matcher,
        api_version=api_version
    )

    am_downtimer.set_silence()
