import datetime
from urllib.parse import urlparse
import requests
import logging


class Matcher:
    def __init__(self, name=None, value=None, is_regex=None):
        self.set_name(name)
        self.set_value(value)
        self.set_is_regex(is_regex)

    def set_name(self, name):
        if name is None:
            name = 'alertname'
        self.name = name

    def get_name(self):
        return self.name

    def set_value(self, value):
        if value is None:
            value = '.*'
        self.value = value

    def get_value(self):
        return self.value

    def set_is_regex(self, is_regex):
        if is_regex is None:
            is_regex = True
        self.is_regex = bool(is_regex)

    def get_is_regex(self):
        return self.is_regex

    def get_as_dict(self):
        return {
            'name': self.get_name(),
            'value': self.get_value(),
            'isRegex': self.get_is_regex(),
        }


class AlertmanagerDowntimer:
    def __init__(self, url,
                 username=None, password=None,
                 start_hour=None, end_hour=None,
                 matcher=None, api_version=None):
        self.set_url(url)
        self.set_username(username)
        self.set_password(password)
        self.set_start_hour(start_hour)
        self.set_end_hour(end_hour)
        self.set_matcher(matcher)
        self.set_api_version(api_version)

        # A null handler ignores all logging messages by default. Thus, if the
        # library is used and logging is never configured, no messages or
        # warningswill appear
        self.logger = logging.getLogger(__name__)
        self.logger.addHandler(logging.NullHandler())

    def set_url(self, url):
        u = urlparse(url)
        if u.scheme and u.netloc:
            self.url = '{}://{}'.format(u.scheme, u.netloc)
        else:
            raise ValueError('url is not valid')

    def get_url(self):
        return self.url

    def set_username(self, username):
        self.username = username

    def get_username(self):
        return self.username

    def set_password(self, password):
        self.password = password

    def get_password(self):
        return self.password

    def set_start_hour(self, start_hour):
        if start_hour is None:
            start_hour = 17
        self.start_hour = int(start_hour)

    def get_start_hour(self):
        return self.start_hour

    def set_end_hour(self, end_hour):
        if end_hour is None:
            end_hour = 9
        self.end_hour = int(end_hour)

    def get_end_hour(self):
        return self.end_hour

    def set_matcher(self, matcher):
        if matcher is None:
            matcher = Matcher()
        self.matcher = matcher

    def get_matcher(self):
        return self.matcher

    def set_api_version(self, api_version):
        if api_version is None:
            api_version = 'v2'
        self.api_version = api_version

    def get_api_version(self):
        return self.api_version

    def get_starts_at(self):
        now = datetime.datetime.utcnow()
        return now.replace(hour=self.get_start_hour(), minute=0, second=0)

    def get_ends_at(self):
        tomorrow = self.get_starts_at() + datetime.timedelta(days=1)
        return tomorrow.replace(hour=self.get_end_hour(), minute=0, second=0)

    def get_auth(self):
        if self.get_username() is not None:
            return (self.get_username(), self.get_password())
        return ()

    def get_silence_api_url(self):
        return '{}/api/{}/silences'.format(
            self.get_url(),
            self.get_api_version(),
        )

    def get_silence_payload(self):
        silence = {
            'matchers': [
                self.get_matcher().get_as_dict(),
            ],
            'startsAt': self.get_starts_at().strftime("%Y-%m-%dT%H:%M:%SZ"),
            'endsAt': self.get_ends_at().strftime("%Y-%m-%dT%H:%M:%SZ"),
            'createdBy': 'alertmanager downtime non working hours',
            'comment': 'Downtime non working hours',
        }
        return silence

    def set_silence(self):
        self.logger.debug(
            "Setting silence with payload: {}".format(
                self.get_silence_payload()
            )
        )
        r = requests.post(
            self.get_silence_api_url(),
            json=self.get_silence_payload(),
            auth=self.get_auth()
        )
        r.raise_for_status()
