# Alertmanager Downtimer

The objectif of this module is to set an Alertmanager's silence.

Silences all alerts on non working hours.

Hours are treated as UTC.

## Env vars

```
export DEBUG=1 # activates verbose mode
export ALERTMANAGER_URL='https://alertmanager.example.com'
export ALERTMANAGER_USERNAME='admin'
export ALERTMANAGER_PASSWORD='p4ssw0rd'
export ALERTMANAGER_START_HOUR=18
export ALERTMANAGER_END_HOUR=9
export MATCHER_NAME='severity'
export MATCHER_VALUE='(warning|critical)'
export MATCHER_IS_REGEX=1
```

## Sample commands

```
./main.py
```

```
ALERTMANAGER_URL='https://alert.ex.co' ./main.py
```

## Pip

To install this package using pip, add the following line in your `requirements.txt` file:

```
-e git+https://gitlab.com/rsicart/alertmanager-downtimer.git@master#egg=alertmanager_downtimer
```

Replace master by a tag or commit if needed.

## Tests

To launch unit tests:

```
python3 -m unittest
```

or

```
python3 -m unittest discover -s ./tests -p "test_*.py"
```

## Cronjob in k8s

The folder `examples/` contains 2 manifests to create:

* a CronJob
* a Secret

The Secret contains HTTP Basic Auth credentials used by Cronjob.

Update `examples/k8s/secret.yaml` or create a Secret manually:

```
kubectl --namespace YOUR_NAMESPACE create secret generic alertmanager-downtimer --from-literal=username=YOUR_USER --from-literal=password='YOUR_PASSWORD'
```

Update `examples/k8s/cronjob.yaml` and apply:

```
kubectl --namespace YOUR_NAMESPACE apply -f examples/k8s/cronjob.yaml
```
